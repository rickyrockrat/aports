# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=ksshaskpass
pkgver=6.1.2
pkgrel=0
pkgdesc="ssh-add helper that uses kwallet and kpassworddialog"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt6-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/ksshaskpass.git"
source="https://download.kde.org/stable/plasma/$pkgver/ksshaskpass-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9a3391fb3bc8fdd515f5b4ae92ad26a983d5d10e6ebd312f5c1f1c774d138a5c9eb902bdb022bdeeb1511253bf133fad122c357abdce8b1221185ad4d545ff20  ksshaskpass-6.1.2.tar.xz
"

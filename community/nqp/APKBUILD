# Contributor: Curt Tilmes <Curt.Tilmes@nasa.gov>
# Maintainer: Curt Tilmes <Curt.Tilmes@nasa.gov>
pkgname=nqp
pkgver=2024.06
pkgrel=0
pkgdesc="Not Quite Perl"
url="https://github.com/Raku/nqp"
arch="all"
options="!archcheck" # Arch dependencies are embedded
license="Artistic-2.0"
depends="moarvm~$pkgver"
makedepends="perl-utils moarvm-dev~$pkgver"
checkdepends="perl-test-harness"
subpackages="$pkgname-doc"
source="https://github.com/Raku/nqp/releases/download/$pkgver/nqp-$pkgver.tar.gz"

build() {
	perl Configure.pl --prefix=/usr --backends=moar
	make -j"$JOBS"
}

check() {
	export TEST_JOBS=$JOBS
	export HARNESS_VERBOSE=1

	msg "Running $TEST_JOBS parallel test jobs"
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dvm644 CREDITS LICENSE README.pod VERSION \
		-t "$pkgdir"/usr/share/doc/"$pkgname"
	cp -vr docs examples "$pkgdir"/usr/share/doc/"$pkgname"/
}

sha512sums="
c6ae15264c593bcbea218677fafa43098a84d758bfe545ffe288dfb445fe6008b7da9b129cbd29933d315b93623e40f9e48937160b81d69cd4480d9d9dcd9121  nqp-2024.06.tar.gz
"

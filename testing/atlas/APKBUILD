# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=atlas
pkgver=0.24.1
pkgrel=0
pkgdesc="Database schema migration tool using modern DevOps principles"
url="https://atlasgo.io/"
# x86, armhf, armv7: multiple packages fail to build on 32-bit platforms due to integer overflow
# riscv64: github.com/remyoudompheng/bigfft fails to build
arch="all !x86 !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="go sqlite-dev"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ariga/atlas/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgver/cmd/atlas"
options="net" # download Go modules

export CGO_ENABLED=1 # required for sqlite driver
export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

# prevent the CLI from checking GitHub tags during build steps
export ATLAS_NO_UPDATE_NOTIFIER=true

build() {
	go build -v -o atlas \
		-ldflags "-X ariga.io/atlas/cmd/atlas/internal/cmdapi.version=v$pkgver"

	for shell in bash fish zsh; do
		./atlas completion $shell > atlas.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 atlas -t "$pkgdir"/usr/bin/

	install -Dm644 atlas.bash \
		"$pkgdir"/usr/share/bash-completion/completions/atlas
	install -Dm644 atlas.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/atlas.fish
	install -Dm644 atlas.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_atlas
}

sha512sums="
dc7f335ec4cd3c687046f4e4af638be3ad7f07ac06aa012c0af82e3092a2f598e4d490ea7fbd0e21ba5feca8b981e439529bbd7b1978911152bf525a28f86fde  atlas-0.24.1.tar.gz
"
